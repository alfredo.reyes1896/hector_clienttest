import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { User } from '../../models/User';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Client } from '../../models/Client';
import { ClientesService } from '../../services/clientes.service';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
}) 
export class LoginComponent implements OnInit {

  public formGroup: FormGroup;
  public clientGroup: FormGroup;

  public user: User = new User();
  
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private clientesService: ClientesService

  ) { }

  ngOnInit() {
    this.buildForm();
    $('.modal').modal();
    $('select').formSelect();
  }


  private buildForm(){
    this.formGroup = this.formBuilder.group({
      email:  ['', [Validators.required, Validators.email]],
      password: ["",[Validators.required, Validators.min(1)]]
    });

    this.clientGroup = this.formBuilder.group({
      nombreCompleto: ['', [Validators.required, Validators.min(1)]],
      direccion:  ['', [Validators.required, Validators.min(1)]],
      edad:  [0,[Validators.required, Validators.min(1)]],
      genero: [1,[Validators.required,]]
    }); 
  
  }


  onSubmit() {
    this.user = this.formGroup.value;
    this.authService.signin(this.user).subscribe((resp: any) => {
      console.log(resp);
      localStorage.setItem("token", resp.idToken.toString());
      this.router.navigateByUrl("/user");
    },(err) =>{
      Swal.fire({
        icon: 'error',
        title: 'Error al autentificar',
        text: 'Verifique su usuario y/o contraseña'
      })     });
  }


  createClient() {
    let newClient = new Client();
    newClient = this.clientGroup.value;
    console.log(newClient);

    this.clientesService.create(newClient).subscribe(
      resp => {
        this.buildForm();
        $("#newClient").modal("close");

        Swal.fire({
          position: 'center',
          icon: 'success',
          title: `Nuevo cliente guardado correctamente`,
          showConfirmButton: false,
          timer: 2000
        }) 
      }, err => {
        console.log(err);
      }
    );
  }
  

}
  