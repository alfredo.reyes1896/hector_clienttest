import { Component, OnInit } from '@angular/core';
import decode from 'jwt-decode';
import { User } from '../../models/User';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CuentasAhorroService } from '../../services/cuentas-ahorro.service';
import { v4 as uuidv4 } from 'uuid';
import { Account } from '../../models/Account';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { Transaction } from '../../models/Transaction';


declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private token:string;
  public newAccountGroup: FormGroup;
  public transactionGroup: FormGroup;
  public idClient: string;
  public newAccount: Account;
  public user: User = new User();
  public accounts: Account[] = [];
  public transaction: Transaction;
  public idAccount: string;
  public typeTransaction: number;
  public transactions: Transaction[];
  public currentAcount: Account;
  public currentTransactions: Transaction[];

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private cuentasAhorroService: CuentasAhorroService,
    private datePipe: DatePipe,

  ) { }

  ngOnInit() {

    this.buildForm();
    $('.modal').modal();
    $('select').formSelect();


    this.token = localStorage.getItem('token');
    let tokenPayload: any = decode(this.token);
    console.log(tokenPayload);
    this.user.email = tokenPayload.email;
    this.idClient = tokenPayload.user_id;
    this.getAccounts();
  }


  private buildForm(){
    this.newAccountGroup = this.formBuilder.group({
      saldo:  [0, [Validators.required]],
    });

    this.transactionGroup = this.formBuilder.group({
      numeroCuenta:  [, [Validators.required]],
      terminal: ["", [Validators.required, Validators.min(1)]],
      monto:  [0, [Validators.required, Validators.min(1)]],
    });


  }
  
  

  logout() {
    this.authService.logout();
    this.router.navigateByUrl("/login");
  }

  openNewAccount() {

    this.newAccount = new Account();
    this.newAccount.fechaUltimaAct = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.newAccount.idCliente = this.idClient;
    this.newAccount.numeroCuenta = uuidv4().split("-")[4];
    $("#newAccount").modal("open");
  }

  createAccount() {
    this.newAccount.saldo = this.newAccountGroup.get("saldo").value;
    this.cuentasAhorroService.create(this.token, this.newAccount).subscribe(
      resp => {
        this.newAccount = new Account();
        $("#newAccount").modal("close");

        this.getAccounts();
        this.buildForm();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: `Cuenta de ahorro creada correctamente`,
          showConfirmButton: false,
          timer: 2000
        })     

        

      }, err=>{console.log(err)}
    );
  }

  getAccounts() {
    this.accounts = [];
    this.cuentasAhorroService.get(this.token).subscribe(
      resp => {
        for (let i in resp) {
          if (resp[i].idCliente == this.idClient) {
            resp[i].saldoInicial = resp[i].saldo;
            this.accounts.push(resp[i]);
          }
        }
        this.getTransactions();
      }, err => {
        console.log(err);
      }
    );
  }

  openTransaction(type) { //1 deposito 2 retiro
    this.typeTransaction = type;
    this.transaction = new Transaction();
    this.transaction.fechaUltimaAct = this.datePipe.transform(new Date(), 'yyyy-MM-dd hh:m:ss');
    this.transaction.usuario = this.idClient;
    console.log(this.transaction);
    (type ==1 )? this.transaction.tipo = "Deposito": this.transaction.tipo = "Retiro";
    $("#transaction").modal("open");
  }

  newTransaction() {
  

    this.transaction.numeroCuenta = this.transactionGroup.get("numeroCuenta").value;
    this.transaction.terminal = this.transactionGroup.get("terminal").value;
    this.transaction.monto =this.transactionGroup.get("monto").value;

    console.log(this.transaction);

    let currentAccount = this.accounts.find(account => account.numeroCuenta == this.transaction.numeroCuenta);

    if (this.typeTransaction==2 && this.transaction.monto > currentAccount.saldo) {
      
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: `El monto a retirar es mayor al saldo`,
        showConfirmButton: true,
        
      })

    } else {

      this.cuentasAhorroService.transaction(this.token, this.transaction).subscribe(
        resp => {
          this.buildForm();
          this.transaction = new Transaction();
          this.getAccounts();
          $("#transaction").modal("close");
  
          if (this.typeTransaction == 1) {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: `Deposito realizado correctamente`,
              showConfirmButton: false,
              timer: 2000
            })   
          } else {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: `Retiro realizado correctamente`,
              showConfirmButton: false,
              timer: 2000
            })   
          }
         
  
        }, err => {
          console.log(err);
        }
      );
         
    }

   
  }


  getTransactions() {
    console.log("getTransaction");
    this.transactions = [];
    this.cuentasAhorroService.getTransacciones(this.token).subscribe(
      respT => {
        console.log(respT);
        for (let i in respT) {
          if (respT[i].usuario == this.idClient) {
            this.transactions.push(respT[i]);
          }
        }
        console.log(this.transactions);


        this.refreshSaltoTotal();

      }, err => {
        
      }
    );
  }

  refreshSaltoTotal() {
    this.accounts.forEach(account => {
      this.transactions.forEach(transaction => {
        if (account.numeroCuenta == transaction.numeroCuenta) {
          if (transaction.tipo == "Deposito") {
            account.saldo = account.saldo + transaction.monto;
          } else {
            account.saldo = account.saldo - transaction.monto; 
          }
        }
      });
    });
  }

  openReport(account:Account) {
    this.currentAcount = account;
    console.log(this.currentAcount);
    this.getTransactionsByAccount();
    $("#report").modal("open");

  }

  getTransactionsByAccount() {
    this.currentTransactions = [];
    this.transactions.forEach(transaction => {
      if (transaction.numeroCuenta == this.currentAcount.numeroCuenta) {
        this.currentTransactions.push(transaction);
      }
    });
    console.log(this.currentTransactions);
  }
}
