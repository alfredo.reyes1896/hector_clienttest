import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from '../models/Account';
import { Transaction } from '../models/Transaction';

@Injectable({
  providedIn: 'root'
})
export class CuentasAhorroService {

  constructor(
    private http: HttpClient,
  ) { }

  get(token: string) :Observable<any> {
    return this.http.get(`https://mibanco-333616-default-rtdb.firebaseio.com/cuentaAhorro/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`);
  }

  create(token: string, account: Account) :Observable<any> {
    return this.http.post(`https://mibanco-333616-default-rtdb.firebaseio.com/cuentaAhorro/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`, account);
  }

  transaction(token: string, transaction: Transaction) {
    return this.http.post(`https://mibanco-333616-default-rtdb.firebaseio.com/transacciones/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`, transaction);
  }

  getTransacciones(token: string) :Observable<any> {
    return this.http.get(`https://mibanco-333616-default-rtdb.firebaseio.com/transacciones/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`);
  }
}
 