import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../models/User';
import { catchError } from 'rxjs/operators';
import { Observable,throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    public jwtHelper: JwtHelperService
  ) { }

  signin(user: any) :Observable<any> {
    user.returnSecureToken = true;
    return this.http.post(`https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyB-p8CKoaQr097NJ8YJRpoWpezJj5xRRUI`, user);
  }

  logout(){
    localStorage.removeItem('token');
  }

  public isAuthenticated(): boolean {
    let token = localStorage.getItem('token');
     if (token != null) {
      return !this.jwtHelper.isTokenExpired(token); 
     } else {
       return false;
   }
  }
 
}
 