export class Client{

    idCliente: number;
    nombreCompleto: string;
    direccion: string;
    edad: number;
    genero: number; 

    constructor() {
        this.idCliente = 0;
        this.nombreCompleto = "";
        this.direccion = "";
        this.edad = 0;
        this.genero = 0;
        
    }
}