export class Transaction{

    fechaUltimaAct: string;
    monto: number;
    numeroCuenta: string;
    terminal: string;
    tipo: string;
    usuario: string;
    
    constructor() {
        this.fechaUltimaAct = "";
        this.monto = 0;
        this.numeroCuenta = "";
        this.terminal = "";
        this.tipo = "";
        this.usuario = "";
        
    }
}