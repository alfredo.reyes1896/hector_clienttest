export class Account{
    estado: string;
    fechaUltimaAct: string;
    idCliente: string;
    numeroCuenta: string;
    saldo: number
    
    constructor() {
        this.estado = "Activa";
        this.fechaUltimaAct = "";
        this.idCliente = "";
        this.numeroCuenta = "";
        this.saldo =0;
    }
}